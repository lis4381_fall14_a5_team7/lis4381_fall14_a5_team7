<?php

if (!isset($_COOKIE['firsttime']))
{
    setcookie("firsttime", "no", 0);
    header('Location: landing.htm');  
}
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Don't Die in Debt</title> 
 
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">
  </head>
<!-- NAVBAR ================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container" style="height: 60px;">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php" style="margin-top:-10px;">Florida State Student <br /> Financial Solutions</a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                
                <!-- HOME -->
                <li class="active"><a href="index.php">Home</a></li>
                <!-- /HOME -->
                
                <!-- ABOUT -->
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">About </a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="about.htm">About</a></li>
                    <li class="divider"></li>
                    <li><a href="about.htm">Mission Statement</a></li>
                    <li><a href="about.htm">Our Story</a></li>
                    </ul>
                </li>
                <!-- /ABOUT -->
                
                <!-- FINANCIAL PLANNING -->
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Financial Planning</a>
                  <ul class="dropdown-menu" role="menu">
                  <li><a href="financial_planning.htm">Financial Planning</a></li>
                    <li class="divider"></li>
                    <li><a href="financial_planning.htm">Basic Budgeting</a></li>
                    <li><a href="financial_planning.htm">Advanced Budgeting</a></li>
                    <li><a href="financial_planning.htm">Smart Investing</a></li>
                    </ul>
                </li>
                <!-- /FINANCIAL PLANNING -->
                
                <!--DEBT CONSOLIDATION -->
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Debt Consolidation</a>
                  <ul class="dropdown-menu" role="menu">
                   <li><a href="debt_consolidation.htm">Debt Consolidation</a></li>
                    <li class="divider"></li>
                    <li><a href="debt_consolidation.htm">Loan</a></li>
                    <li><a href="debt_consolidation.htm">Debt Calculator</a></li>
                    <li><a href="debt_consolidation.htm">Credit Cards</a></li>
                    <li><a href="debt_consolidation.htm">Credit Recovery</a></li>
                    </ul>
                </li>
                <!-- /DEBT CONSOLIDATION-->
                
                <!-- LOAN -->
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Loan</a>
                  <ul class="dropdown-menu" role="menu"> 
                  <li><a href="loan.htm">Loan</a></li>
                    <li class="divider"></li>
                    <li><a href="loan.htm">Federal vs Private</a></li>
                    <li><a href="loan.htm">Needs</a></li>
                    <li><a href="loan.htm">Actual Costs</a></li>
                    </ul>
                </li>
                 <!--/LOAN -->
                
                <!-- TERMS & CONDITIONS 
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Terms & Conditions</a>
                  <ul class="dropdown-menu" role="menu">
                   <li><a href="terms_conditions.htm">Terms & Conditions</a></li>
                    <li class="divider"></li>
                    <li><a href="#Disclaimer">Disclaimer</a></li>
                    <li><a href="#Policies">Policies</a></li>
                    </ul>
                </li>
                 /TERMS & CONDITIONS -->
                
                <!-- CONTACT 
                <li><a href="contact.htm">Contact</a></li>
                 /CONTACT -->
                
                <!-- CUSTOMER SERVICE 
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Customer Service</a>
                  <ul class="dropdown-menu" role="menu">
                   <li><a href="customer_service.htm">Customer Service</a></li>
                    <li class="divider"></li>
                     <li><a href="#Payment_Options">Payment Options</a></li> 
                    <li><a href="#Privacy_Security">Privacy & Security</a></li>
                    <li><a href="#Support">24/7 Support</a></li>
                    </ul>
                </li>
                 /CUSTOMER SERVICE -->
                
                <!-- PROMOS 
                <li><a href="promos.htm">Promos</a></li>
                 /PROMOS -->
                
                 <!-- EXTERNAL LINKS & RESOURCES -->
                
                   <li><a href="additional_resources.htm">External Links & Resources</a></li>
                  
                <!-- /EXTERNAL LINKS & RESOURCES -->
                
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>


    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
      </ol>
      <div class="carousel-inner">
        <div class="item active">
          <img src="images/9.jpg" alt="Financial debt">
          <div class="container">
            <div class="carousel-caption">
              
              
            </div>
          </div>
        </div>
        <div class="item">
          <img src="images/11.jpg" alt="Student debt">
          <div class="container">
            <div class="carousel-caption">
              
              
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>
    <!-- /.carousel -->



    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">
    <div class="well text-center">
                   Florida State Student Financial Solutions is the perfect place to start on your path to financial responsibility.
                  Florda State Student Financial Solutions is an easy way to spear debt!
                </div>

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4">
         
          <img class="img-circle" src="images/5.jpg" alt="Financial planning" style="width: 140px; height: 140px;">
          
          <h2>Financial Planning</h2>
            <a href="landing.htm">landing page link</a>
          <p>Planning your finances both now and in your future is the key to a comfortable lifestyle. To learn more about financial 
            planning and to utilize our tools and information click on the button below!
          </p>
          
          <p><a class="btn btn-default" href="financial_planning.htm" role="button">Learn More &raquo;</a></p>
        
          
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          
          <img class="img-circle" src="images/13.jpg" alt="Debt" style="width: 140px; height: 140px;">
          
          <h2>Debt Consolidation</h2>
          <p>With tuition rates on the rise student debt is almost unavoidable. Click below to use our debt consolodation tools 
            and get on your way to being debt free!
          </p>
          
          <p><a class="btn btn-default" href="debt_consolidation.htm" role="button">More Details Here &raquo;</a></p>
        
          
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          
          <img class="img-circle" src="images/1.jpg" alt="Loans" style="width: 140px; height: 140px;">
         
          <h2>Loans</h2>
          <p>Choosing the correct loan can be the differece between being debt free in years versus being debt free in decades.
            Click below to learn important rules and suggestions for choosing a loan.
          </p>
          
          <p><a class="btn btn-default" href="loan.htm" role="button">Find Out More &raquo;</a></p>
       
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->



      <!-- Let's not use the features below. Too much info needed and the features aboveare adequate I believe. -->
     
      <!-- START THE FEATURETTES 

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">First featurette heading. <span class="text-muted">It'll blow your mind.</span></h2>
          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-5">
          <img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
        </div>
        <div class="col-md-7">
          <h2 class="featurette-heading">Oh yeah, it's that good. <span class="text-muted">See for yourself.</span></h2>
          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">And lastly, this one. <span class="text-muted">Checkmate.</span></h2>
          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider"> -->

      <!-- /END THE FEATURETTES -->


      <!-- FOOTER -->
      <footer>
      
        <p>
          <a href="terms_conditions.htm">Terms & Conditions</a> &middot;
          <a href="customer_service.htm"> Customer Service</a> &middot;
          <a href="contact.htm">Contact</a> 
          <br />
          445 Seminole Way, Tallahassee FL 32317 | (850) 421-9984 | help@idontwanttodieindebt.com
          <br />
          &copy; 2014 FSSFS. Web Author: FSSFS Group. Updated: October 2014.
          <a href="http://validator.w3.org/check?uri=joebarrentine.com%2FFSSFS&charset=utf-8&doctype=HTML5&group=0">Validator</a>
        </p>
        
      </footer>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
